@extends('home.master')
@section('konten')
  <div class="container mtp-1">
    <div class="row ">
      <div class="col-md-4 mt-4">
        <img src="/image/ml.jpg"  alt="" class="img-fluid">
          <div class="judul-item mt-2">
            Mobile Lagend
          </div>
          <div class="item-conten mt-2">
            Perhatian : Lorem ipsum dolor sit amet consectetur adipisicing elit. Alias quisquam harum dolorum dolores hic molestiae nemo! Aspernatur vitae doloribus laudantium dolores rerum illo accusamus consectetur. Sed unde excepturi error quidem!
          </div>
      </div>
      <div class="col-md-8 mt-4">

          <div class="border-item-1">
              <div class="judul-item-1 ps-3 pt-2">
                  Pilih voucher
              </div>
              <div class="row m-3 d-flex justify-content-md-center justify-content-center">

                <div class="card m-3" style="width: 12rem;">
                  <div class="card-body">
                    <p class="card-text text-center">30 Hari Diamond Mobile</p>
                  </div>
                </div>
                <div class="card m-3" style="width: 12rem;">
                  <div class="card-body">
                    <p class="card-text text-center">30 Hari Diamond All Screen</p>
                  </div>
                </div>
                <div class="card m-3" style="width: 12rem;">
                  <div class="card-body">
                    <p class="card-text text-center">12 Bulan Diamond Mobile</p>
                  </div>
                </div>
              </div>
          </div>

          <div class="border-item-1">
            <div class="judul-item-1 ps-3 pt-2">
                Pilih Metode Pembayaran
            </div>
            <div class="row m-3 d-flex justify-content-md-center justify-content-center">

              <div class="card m-2" >
                <div class="card-body">
                  <p class="card-text">30 Hari Diamond Mobile</p>
                </div>
              </div>

              <div class="card m-2" >
                <div class="card-body">
                  <p class="card-text">30 Hari Diamond Mobile</p>
                </div>
              </div>

              <div class="card m-2" >
                <div class="card-body">
                  <p class="card-text">30 Hari Diamond Mobile</p>
                </div>
              </div>
           
            </div>
        </div>


        <div class="border-item-1">
          <div class="judul-item-1 ps-3 pt-2">
              Beli
          </div>
          <div class="row m-3 d-flex justify-content-md-center justify-content-center">

            <div class="card m-2" >
              <div class="card-body">
                <p class="card-text">30 Hari Diamond Mobile</p>
              </div>
            </div>

          </div>
      </div>

      </div>
    </div>
  </div>
@endsection